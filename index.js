/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

		function firstFunction(){
		let fullName = prompt("Enter your full name: "); 
		let age = prompt("Enter your age: "); 
		let location = prompt("Enter your location: ");

		console.log("Hello, " + fullName); 
		console.log("You are " + age + " years old."); 
		console.log("You live in " + location); 
	};
		firstFunction();


/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

		function displayBand(){
			let firstBand = ("1. The Beatles")
			let secondBand = ("2. Metallica")
			let thirdBand = ("3. The Eagles")
			let fourthBand = ("4. L'arc~en~Ciel")
			let fifthBand = ("5. Eraserheads")
			
			console.log(firstBand);
			console.log(secondBand);
			console.log(thirdBand);
			console.log(fourthBand);
			console.log(fifthBand)

		};

			displayBand();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

		function displayMovie(){
			let firstMovie = ("1. The Godfather");
			let secondMovie = ("2. The Godfather, Part II");
			let thirdMovie = ("3. Shawshank Redepemtion");
			let fourtMovie = ("4. To Kill a Mockingbird");
			let fifthMovie = ("5. Psycho");
			let rating1 = ("Rotten Tomatoes Rating: 97%");
			let rating2 = ("Rotten Tomatoes Rating: 96%");
			let rating3 = ("Rotten Tomatoes Rating: 91%");
			let rating4 = ("Rotten Tomatoes Rating: 93%");
			let rating5 = ("Rotten Tomatoes Rating: 96%");
			
			console.log(firstMovie);
			console.log(rating1);

			console.log(secondMovie);
			console.log(rating2);

			console.log(thirdMovie);
			console.log(rating3);

			console.log(fourtMovie);
			console.log(rating4);

			console.log(fifthMovie)
			console.log(rating5);

		};

			displayMovie();
/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();